---
layout: page
title: Contacto
permalink: /Contacto/
---

Hola, soy Laura, @lormez16 en las redes, y aquí en **Lormez Audio Studio** reuno todo el trabajo que hago relacionado con los podcast.


Actualmente produzco tres podcast: *Vacía tu bandeja*, *Los SuperLunes* y *Experimenta con Jekyll*. Si no los conoces te invito a escucharlos y a que me envíes tus comentarios.


También colaboro en **La Unión Podcastera**, una Fraternidad de Podcaster, promocionando y apoyando el podcast en español.

<br>
<hr/>
## Trabaja conmigo

Si tienes una idea que quieres convertir en podcast y no tienes claro como se hace, cuéntamela y juntos la pondremos en marcha.

Si ya eres podcaster y quieres que colabore contigo no dudes en enviarme tu propuesta.  

<br>

Puedes escribirme al correo: **lormez16@protonmail.com** o enviarme un mensaje por Twitter o Telegram buscando por **@lormez16**.
